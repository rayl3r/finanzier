#include "table.h"
#include "config/table_config.h"


table::table()
{
  get_every_entry();

  draw_every_colomn();

  init_columns();

  init_actions();

  init_pop_up();
}

void table::init_columns()
{
  m_treeviewcolumn_ammount.set_title("Amount");
  m_treeviewcolumn_ammount.pack_start(m_cellrenderer_ammount);
  append_column(m_treeviewcolumn_ammount);

  m_treeviewcolumn_reason.set_title("Reason");
  m_treeviewcolumn_reason.pack_start(m_cellrenderer_reason);
  append_column(m_treeviewcolumn_reason);

  append_column("Date", m_table_colomns.m_col_date);

  m_treeviewcolumn_ammount.set_cell_data_func(m_cellrenderer_ammount,sigc::mem_fun(*this, &table::treeviewcolumn_ammount_on_cell_data) );
  m_cellrenderer_ammount.property_editable() = true;
  m_cellrenderer_ammount.signal_editing_started().connect(sigc::mem_fun(*this, &table::cellrenderer_ammount_on_editing_started));
  m_cellrenderer_ammount.signal_edited().connect( sigc::mem_fun(*this, &table::cellrenderer_ammount_on_edited) );

  m_treeviewcolumn_reason.set_cell_data_func(m_cellrenderer_ammount,sigc::mem_fun(*this, &table::treeviewcolumn_reason_on_cell_data) );
  m_cellrenderer_reason.property_editable() = true;
  m_cellrenderer_reason.signal_editing_started().connect(sigc::mem_fun(*this, &table::cellrenderer_reason_on_editing_started));
  m_cellrenderer_reason.signal_edited().connect( sigc::mem_fun(*this, &table::cellrenderer_reason_on_edited) );
}

void table::init_actions()
{
  m_refActionGroup = Gio::SimpleActionGroup::create();
  m_remove_action = m_refActionGroup->add_action("remove", sigc::mem_fun(*this, &table::remove_entry));

  insert_action_group("entry_pop_up", m_refActionGroup);
}

void table::init_pop_up()
{
  std::string infile = POP_UP_CONFIG_TABLE;
  const char* ui_info = infile.c_str();

  m_refBuilder->add_from_string(ui_info);
  Glib::RefPtr<Glib::Object> object = m_refBuilder->get_object("entry_pop_up");
  Glib::RefPtr<Gio::Menu> gmenu = Glib::RefPtr<Gio::Menu>::cast_dynamic(object);

  m_pop_up_menu = new Gtk::Menu(gmenu);
}

table::type_signal_budget table::signal_budget()
{
  return m_signal_budget;
}

void table::update_budget()
{
  m_signal_budget.emit();
}

void table::remove_entry()
{
  auto select = get_selection();
  auto row_iter = select->get_selected();

  if (row_iter)
  {
    auto number = row_iter->get_value(m_table_colomns.m_col_number);
    m_refTreeModel->erase(row_iter);
    set_model(m_refTreeModel);

    m_saver.remove_entry(number);

    update_budget();

    get_every_entry();

    queue_draw();

    return;
  }

  return;
}

void table::get_every_entry()
{

    list_entrys.clear();

    int last_element = m_reader.get_last_entry_number();

    std::vector<std::string> entry;

    for (int i = 1; i< last_element; i++)
    {
      entry.push_back(m_reader.read_ammount(std::to_string(i)));
      entry.push_back(m_reader.read_reason(std::to_string(i)));
      entry.push_back(m_reader.read_date(std::to_string(i)));
      entry.push_back(std::to_string(i));

      list_entrys.push_back(entry);

      entry.clear();      
    }
}

void table::draw_every_colomn()
{
  m_refTreeModel = Gtk::ListStore::create(m_table_colomns);
  set_model(m_refTreeModel);

  for (auto const &entry : list_entrys)
  {
    Gtk::TreeModel::Row row = *(m_refTreeModel->append());
    row[m_table_colomns.m_col_ammount] = entry[0];
    row[m_table_colomns.m_col_reason] = entry[1];
    row[m_table_colomns.m_col_date] = entry[2];
    row[m_table_colomns.m_col_number] = entry[3];
  }
}

void table::treeviewcolumn_reason_on_cell_data(
        Gtk::CellRenderer* /* renderer */,
        const Gtk::TreeModel::iterator& iter)
{
  if(iter)
  {
    Gtk::TreeModel::Row row = *iter;
    std::string model_value = row[m_table_colomns.m_col_reason];

    Glib::ustring view_text = model_value;
    m_cellrenderer_reason.property_text() = view_text;
  }
}

void table::cellrenderer_reason_on_editing_started(
        Gtk::CellEditable* cell_editable, const Glib::ustring& /* path */)
{
  if(m_validate_retry)
  {
    auto celleditable_validated = cell_editable;

    auto pEntry = dynamic_cast<Gtk::Entry*>(celleditable_validated);
    if(pEntry)
    {
      pEntry->set_text(m_invalid_text_for_retry);
      m_validate_retry = false;
      m_invalid_text_for_retry.clear();
    }
  }

}

void table::cellrenderer_reason_on_edited(
        const Glib::ustring& path_string,
        const Glib::ustring& new_text)
{
  Gtk::TreePath path(path_string);

  std::string new_value = new_text;

  Gtk::TreeModel::iterator iter = m_refTreeModel->get_iter(path);

  int column_number = std::stoi(m_refTreeModel-> get_string(iter)) + 1;

  m_saver.edit_reason(std::to_string(column_number), new_value);

  if(iter)
  {
      Gtk::TreeModel::Row row = *iter;

  row[m_table_colomns.m_col_reason] = new_value;
  }
}

void table::treeviewcolumn_ammount_on_cell_data(
        Gtk::CellRenderer* /* renderer */,
        const Gtk::TreeModel::iterator& iter)
{
  if(iter)
  {
    Gtk::TreeModel::Row row = *iter;
    std::string model_value = row[m_table_colomns.m_col_ammount];

    Glib::ustring view_text = model_value;
    m_cellrenderer_ammount.property_text() = view_text;
  }
}

void table::cellrenderer_ammount_on_editing_started(
        Gtk::CellEditable* cell_editable, const Glib::ustring& /* path */)
{
  if(m_validate_retry)
  {
    auto celleditable_validated = cell_editable;

    auto pEntry = dynamic_cast<Gtk::Entry*>(celleditable_validated);
    if(pEntry)
    {
      pEntry->set_text(m_invalid_text_for_retry);
      m_validate_retry = false;
      m_invalid_text_for_retry.clear();
    }
  }

}

void table::cellrenderer_ammount_on_edited(
        const Glib::ustring& path_string,
        const Glib::ustring& new_text)
{
  Gtk::TreePath path(path_string);

  std::string new_value = new_text;
  if (flib::is_digit(new_value))
  {
    Gtk::TreeModel::iterator iter = m_refTreeModel->get_iter(path);

    int column_number = std::stoi(m_refTreeModel-> get_string(iter)) + 1;

    m_saver.edit_ammount(std::to_string(column_number), new_value);

    update_budget();

    if(iter)
    {
      Gtk::TreeModel::Row row = *iter;

      row[m_table_colomns.m_col_ammount] = new_value;
  }
  }
}

void table::redraw()
{
  get_every_entry();

  draw_every_colomn();

  queue_draw();
}

bool table::on_button_press_event(GdkEventButton* button_event)
{
  if (button_event->button == 3)
  {   
      button_event->button = 1;
      Gtk::TreeView::on_button_press_event(button_event);
      button_event->button = 3;
      if(!m_pop_up_menu->get_attach_widget())
          m_pop_up_menu->attach_to_widget(*this);

      m_pop_up_menu->popup(button_event->button, button_event->time);
      return true;
  }

  return Gtk::TreeView::on_button_press_event(button_event);
}
#include "initialisation.h"

using namespace std;

initialisation::initialisation()
{
    //init the time
    time_t now = time(0);    
    tm *dt = localtime(&now);

    //calculate the current year
    this_year = 1900 + dt->tm_year;
    logger::log_debug(std::to_string(this_year));

    //calculate the current month
    int i_this_month = 1 + dt->tm_mon;
    this_month = flib::switch_month(i_this_month);
    logger::log_debug(this_month);

    save_folder = SAVE_PATH;
}

initialisation::~initialisation()
{

}

void initialisation::set_save_folder(std::string new_save_folder)
{
    save_folder = new_save_folder;
    //run();
}

void initialisation::run()
{
    logger::log_debug("Starting initialisation.");
    create_save_folder();
    create_month_folder();
    create_save_files();
}

void initialisation::create_save_folder()
{   
    if(mkdir(save_folder.c_str(),0777) != -1)
    {
        logger::log_info("created save folder.");
    }
    else
    {
        logger::log_info("save folder already exists.");
    }
}

void initialisation::create_month_folder()
{
    string str_year = to_string(this_year);
    string m_dir = save_folder + "/" + str_year;
    const char* dir = m_dir.c_str();

    if(mkdir(dir,0777) != -1)
    {
        logger::log_info("created year folder.");
    }
    else
    {
        logger::log_info("year folder exists.");
    }
}


void initialisation::create_save_files()
{   
    string save_to = save_folder + "/" + to_string(this_year) + "/" + this_month + ".xml";

    if (!flib::file_exists(save_to.c_str()))
    {
        logger::log_info("created month file.");

        std::ofstream outfile(save_to);
        outfile << "<entrys></entrys>" << std::endl;

        xmlpp::DomParser parser;
        parser.parse_file(save_to);
        auto doc = parser.get_document();

        auto root_node = doc->get_root_node();
        auto child_node =root_node->add_child_element("entry");
        auto entry_number = child_node->add_child_element("number");

        entry_number->add_child_text("0");
        doc->write_to_file(save_to);        
    }
    else
    {
        logger::log_info("monthy file exists.");
    }


    save_to = save_folder + "/" + to_string(this_year) + "/vars.xml";

    if (!flib::file_exists(save_to.c_str()))
    {
        logger::log_info("created vars file");

        std::ofstream outfile(save_to);
        outfile << "<vars></vars>" << std::endl;

        xmlpp::DomParser parser;
        parser.parse_file(save_to);
        auto doc = parser.get_document();

        auto root_node = doc->get_root_node();
        auto child_node =root_node->add_child_element("budget");
        auto month_node = child_node->add_child_element(this_month);
        month_node->add_child_text("0");

        doc->write_to_file(save_to);        
    }
    else
    {
        logger::log_info("vars file exists.");

        xmlpp::DomParser parser;
        parser.parse_file(save_to);
        auto doc = parser.get_document();

        auto root_node = doc->get_root_node();
        auto budget_node = root_node->get_first_child("budget");
        auto first_month = budget_node->get_first_child();
        auto budget_element = first_month->get_parent();
        auto month_node = budget_element->get_first_child(this_month);

        if (month_node == nullptr)
        {
            auto new_month_node = budget_element->add_child_element(this_month);
            new_month_node->add_child_text("0");
            doc->write_to_file(save_to);
        }
        else
        {
            logger::log_warning("in the vars file the month node is faulty.");
        }
    }

    save_to = save_folder + "/" + to_string(this_year) + "/monthly.xml";

    if (!flib::file_exists(save_to.c_str()))
    {
        logger::log_info("created monthly file");

        std::ofstream outfile(save_to);
        outfile << "<monthly></monthly>" << std::endl;

        xmlpp::DomParser parser;
        parser.parse_file(save_to);
        auto doc = parser.get_document();

        auto root_node = doc->get_root_node();
        auto month_node = root_node->add_child_element(this_month);
        auto entry_node = month_node->add_child_element("entry");
        auto entry_number = entry_node->add_child_element("number");

        entry_number->add_child_text("0");

        doc->write_to_file(save_to);
        
    }
    else
    {
        logger::log_info("monthly file exists.");

        xmlpp::DomParser parser;
        parser.parse_file(save_to);
        auto doc = parser.get_document();

        auto root_node = doc->get_root_node();
        auto month_node = root_node->get_first_child(this_month);

        if (month_node == nullptr)
        {
            auto month_node = root_node->add_child_element(this_month);

            auto entry_node = month_node->add_child_element("entry");

            auto entry_number = entry_node->add_child_element("number");

            entry_number->add_child_text("0");
        }
        else
        {
            logger::log_warning("in the monthly file the month node is faulty.");
        }
        
    }
}
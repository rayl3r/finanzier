#include "budget_area.h"


budget_area::budget_area()
{
    set_size_request(100,50);
    update();
}

void budget_area::update()
{
    budget = m_calc.get_budget();
    summed = m_calc.sum_up_ammounts();
}

bool budget_area::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    Gtk::Allocation allocation = get_allocation();
    const int width = allocation.get_width();
    const int height = allocation.get_height();

    cr->set_source_rgb(1, 1, 1);
    cr->move_to(0, 0);
    cr->line_to(width, 0);
    cr->line_to(width, height);
    cr->line_to(0, height);
    cr->line_to(0, 0);
    cr->stroke();

    cr->set_source_rgb(0.18, 0.9, 0.35);
    draw_text(cr,width/2,0,std::to_string(budget));
    cr->set_source_rgb(1, 1, 1);
    draw_text(cr,0,height*2/3,"  =");
    draw_text(cr,width/2,height*2/3,flib::cut_down(std::to_string(budget-summed)));
    cr->set_source_rgb(1, 0.18, 0.18);
    draw_text(cr,0,height/3,"  -");
    draw_text(cr,width/2,height/3,flib::cut_down(std::to_string(summed)));

    return true;
}

void budget_area::redraw()
{
    Gtk::Allocation allocation = get_allocation();
    const int width = allocation.get_width();
    const int height = allocation.get_height();

    update();
    queue_draw_area(0,0,width,height);
}

void budget_area::draw_text(const Cairo::RefPtr<Cairo::Context>& cr, int pos_width, int pos_height, std::string text)
{
  Pango::FontDescription font;

  font.set_family("Monospace");
  font.set_weight(Pango::WEIGHT_BOLD);

  auto layout = create_pango_layout(text);

  layout->set_font_description(font);

  int text_width = 10;
  int text_height = 10;

  layout->get_pixel_size(text_width, text_height);

  cr->move_to(pos_width-text_width/2, pos_height);

  layout->show_in_cairo_context(cr);
  
}
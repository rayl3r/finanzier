#include "dialogs.h"

budget_dialog::budget_dialog()
{

    set_title("change budget");

    init_buttons();

    set_resizable(false);

    ok_button.set_label("ok");
    cancel_button.set_label("cancel");

    button_box.set_spacing(5);
    button_box.add(ok_button);
    button_box.add(cancel_button);

    get_vbox()->set_spacing(5);
    get_vbox()->add(budget_entry);
    get_vbox()->add(button_box);

    budget_entry.activate();

    set_size_request(get_vbox()->get_allocated_width(), get_vbox()->get_allocated_height());

    show_all_children();
}

void budget_dialog::init_buttons()
{
    ok_button.signal_clicked().connect(sigc::mem_fun(*this, &budget_dialog::change_budget));

    cancel_button.signal_clicked().connect(sigc::mem_fun(*this, &budget_dialog::close));
}

void budget_dialog::close()
{   
    logger::log_info("Closed budget dialog.");
    budget_entry.set_text("");
    hide();
}

void budget_dialog::change_budget()
{
    std::string current_budget = budget_entry.get_text();

    if (!current_budget.empty())
    {
        if (flib::is_digit(current_budget.c_str()))
        {
            saver.set_budget(current_budget);
            budget_entry.set_text("");

            set_title("change budget");
            close();
        }
        else
        {   
            set_title("Please insert a number");
        }
    }
    else
    {
        set_title("empty input");
    }
    
}

bool budget_dialog::on_key_press_event(GdkEventKey* key_event)
{   
    if(key_event->keyval == 65293)
    {
        change_budget();

        return true;
    }
    else if (key_event->keyval == GDK_KEY_Escape)
    {
        close();

        return true;
    }

    return Gtk::Window::on_key_press_event(key_event);
}



monthly_dialog::monthly_dialog()
{
    set_title("add monthly");
    set_resizable(false);

    init();

    show_all_children();
}

void monthly_dialog::init()
{
    cancel_button.add_label("Cancel");
    add_button.add_label("Add");

    button_box.set_spacing(5);
    button_box.add(cancel_button);
    button_box.add(add_button);

    add_button.signal_clicked().connect(sigc::mem_fun(*this, &monthly_dialog::add_monthly));
    cancel_button.signal_clicked().connect(sigc::mem_fun(*this, &monthly_dialog::close));

    entry_box.set_spacing(5);
    amount_entry.set_placeholder_text("Amount");
    reason_entry.set_placeholder_text("Reason");
    entry_box.add(amount_entry);
    entry_box.add(reason_entry);

    get_vbox()->set_spacing(5);
    get_vbox()->add(entry_box);
    get_vbox()->add(button_box);

    set_size_request(get_vbox()->get_allocated_width(), get_vbox()->get_allocated_height());
}

void monthly_dialog::close()
{
    
    logger::log_info("Closed monthly dialog.");
    amount_entry.set_text("");
    reason_entry.set_text("");
    hide();
}

void monthly_dialog::add_monthly()
{
    std::string amount = amount_entry.get_text();
    std::string reason = reason_entry.get_text();

    if(!reason.empty())
    {
        if(!amount.empty())
        {
            m_saver.add_monthly(amount,reason);
        }
    }
    
    close();
}

bool monthly_dialog::on_key_press_event(GdkEventKey* key_event)
{   

    if(key_event->keyval == 65293)
    {
        add_monthly();

        return true;
    }
    if (key_event->keyval == GDK_KEY_Escape)
    {
        close();

        return true;
    }

    return Gtk::Window::on_key_press_event(key_event);
}
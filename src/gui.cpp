#include "gui.h"

gui::gui() : main_box(Gtk::ORIENTATION_HORIZONTAL),
              Entry_boxes(Gtk::ORIENTATION_HORIZONTAL),
               left_side_bar(Gtk::ORIENTATION_VERTICAL),
               middle_bar(Gtk::ORIENTATION_VERTICAL)
{

    init_buttons_signal();

    std::string name = "finanzier";
    std::string version = VERSION_STR;
    std::string title = name + " " + version;

    //set window params
    set_title(title);
    set_resizable(true);
    set_border_width(5);

    signal_check_resize().connect(sigc::mem_fun(*this, &gui::resize_all));

    //init the button and labels
    init_buttons_labels();

    //init the scrollable Window
    init_scrolled_window();

    //init is in the vertical button box
    init_left_side_bar();

    //init the horizontal button box
    init_Entry_boxes();

    //init the Horizontal box
    init_middle_bar();

    //init the main box
    init_main_box();

    add(main_box);

    show_all_children();
}


void gui::init_scrolled_window()
{
    m_ScrolledWindow.add(m_table);

    m_monthlyScrolledWindow.set_min_content_width(150);

    m_monthlyScrolledWindow.add(m_monthy_table);
}

void gui::init_main_box()
{
    main_box.set_spacing(10);
    main_box.add(middle_bar);
    main_box.add(left_side_bar);
}

void gui::init_middle_bar()
{
    middle_bar.set_spacing(4);
    middle_bar.add(Entry_boxes);
    middle_bar.add(m_ScrolledWindow);
}

void gui::init_Entry_boxes()
{   
    reson_box.add(reason_label);
    reson_box.add(reason_Entry);

    ammount_box.add(ammount_label);
    ammount_box.add(ammount_Entry);

    Entry_boxes.set_spacing(20);
    Entry_boxes.add(ammount_box);
    Entry_boxes.add(reson_box);
    Entry_boxes.add(add_Entry);
}

void gui::init_left_side_bar()
{
    budget_box.add(m_budget_area);

    left_side_bar.set_spacing(10);
    left_side_bar.add(budget_box);
    left_side_bar.add(change_budget);

    left_side_bar.add(m_monthlyScrolledWindow);

    left_side_bar.add(add_monthly_button);
}

void gui::init_buttons_labels()
{
    add_Entry.add_label("Add");
    add_Entry.set_border_width(10);
    change_budget.add_label("Change Budget");
    add_monthly_button.add_label("Add monthly");

    budget.set_text(m_reader.get_budget());
    summed_budget.set_text(std::to_string(m_calculator.get_budget() - m_calculator.sum_up_ammounts()));
    expenses_label.set_text(std::to_string(m_calculator.sum_up_ammounts()));
    plus_sign.set_text("-");
    budget_text.set_text("Budget");
    ammount_label.set_text("Amount");
    reason_label.set_text("Reason");
}

void gui::init_buttons_signal()
{
    add_Entry.signal_clicked().connect(sigc::mem_fun(*this, &gui::add_to_entrys));
    change_budget.signal_clicked().connect(sigc::mem_fun(*this, &gui::show_budget_dialog));
    add_monthly_button.signal_clicked().connect(sigc::mem_fun(*this, &gui::show_monthly_dialog));
}

void gui::resize_all()
{
    int width, height;
    get_size(width,height);

    int height_entrys = Entry_boxes.get_height();
    int width_right_bar = left_side_bar.get_width();
    int left_side_button_height = budget_box.get_height() + change_budget.get_height() + add_monthly_button.get_height();

    m_ScrolledWindow.set_min_content_width(width - width_right_bar - 30);

    m_ScrolledWindow.set_min_content_height(height - height_entrys - 20);
    m_monthlyScrolledWindow.set_min_content_height(height - left_side_button_height - 50);

}

void gui::show_budget_dialog()
{
    logger::log_info("Opened budget dialog.");
    enter_budget.run();

    budget.set_text(m_reader.get_budget());
    update_budget();
}

void gui::show_monthly_dialog()
{
    logger::log_info("Opened monthly dialog.");
    m_monthly_dialog.run();

    m_monthy_table.redraw();
}

void gui::update_budget()
{
    m_budget_area.redraw();
}

void gui::add_to_entrys()
{
    std::string amount = ammount_Entry.get_text();
    std::string reason = reason_Entry.get_text();

    if (!amount.empty() && !reason.empty())
    {
        if (flib::is_digit(amount))
        {
            ammount_Entry.set_text("");
            reason_Entry.set_text("");

            m_saver.add_entry(amount, reason);

            //reset everything that might changed after false inputs
            ammount_label.set_text("Amount");
            ammount_Entry.set_placeholder_text("");
            reason_Entry.set_placeholder_text("");

            m_table.redraw();
        }
        else
        {
            logger::log_info("No number as amount.");
            ammount_label.set_text("No Number");
        }
    }
    else
    {
        logger::log_info("One entry is missing.");
        ammount_Entry.set_placeholder_text("Entry missing");
        reason_Entry.set_placeholder_text("Entry missing");
    }
    update_budget();
}

void gui::close()
{
    logger::log_info("Closed gui");
    hide();
}

bool gui::on_key_press_event(GdkEventKey* key_event)
{
    if(key_event->keyval == 65293)
    {
        add_to_entrys();
        return true;
    }

    update_budget();

    return Gtk::Window::on_key_press_event(key_event);
}

#include "gui.h"
#include "initialisation.h"


int start_gui(int argc, char *argv[])
{
    logger::log_info("Starting gui");
    auto app = Gtk::Application::create(argc, argv, "org.gtkmm.examples.base");

    gui window;

    window.m_table.signal_budget().connect(sigc::mem_fun(window, &gui::update_budget));
    window.m_monthy_table.signal_budget().connect(sigc::mem_fun(window, &gui::update_budget));

    window.show_all();

    return app->run(window);
}


int main(int argc, char *argv[])
{
    initialisation ini;

    ini.run();

    start_gui(argc,argv);
}
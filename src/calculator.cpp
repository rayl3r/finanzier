#include "calculator.h"


float calculator::sum_up_ammounts()
{
    get_all_ammounts();

    float sum = 0;

    for (auto &ammount : list_ammounts)
    {
        sum += ammount;
    }

    auto every_monthly = m_reader.get_every_monthly();

    for(auto const &entry : every_monthly)
    {
        sum += flib::convert_to_int_times_hundret(entry[0])/100;
    }

    return sum;
}

void calculator::get_all_ammounts()
{      
    list_ammounts.clear();

    std::string string_amount;
    float int_ammount;

    for(int i = 1; i< m_reader.get_last_entry_number(); i++)
    {   
        string_amount = m_reader.read_ammount(std::to_string(i));
        int_ammount = flib::convert_to_int_times_hundret(string_amount);

        list_ammounts.push_back(int_ammount/100);
    }
}

int calculator::get_budget()
{
    return std::stoi(m_reader.get_budget());
}

void calculator::change_save_folder(std::string new_save_folder)
{
    m_reader.set_save_folder(new_save_folder);
}
#include "save_read.h"
#include <ctime>

using namespace std;


save_read::save_read()
{
    //init the time
    time_t now = time(0);    
    tm *dt = localtime(&now);

    //calculate the current year
    this_year = 1900 + dt->tm_year;
    //cout << "this year : "<< this_year << endl;

    //calculate the current month
    int i_this_month = 1 + dt->tm_mon;
    this_month = flib::switch_month(i_this_month);
    //cout << "this month : "<<this_month << endl;

    //calculate the current day
    int this_day = dt->tm_mday;
    //cout << "today : "<< this_day << endl;

    save_folder = SAVE_PATH;

    save_file = save_folder + "/" + to_string(this_year) + "/" + this_month + ".xml";    

    date = to_string(this_day) + "." + to_string(i_this_month) + "." + to_string(this_year);

    vars_file = save_folder + "/" + std::to_string(this_year) + "/vars.xml";

    month_file = save_folder + "/" + std::to_string(this_year) + "/monthly.xml";

}   

void save::add_entry(string ammount, string reason)
{   
    xmlpp::DomParser parser;
    parser.parse_file(save_file);
    auto doc = parser.get_document();

    auto root_node = doc->get_root_node();
    auto new_child = root_node->add_child_element("entry");
    auto last_child = new_child->get_previous_sibling();

    if (!last_child)
    {
        logger::log_warning("Month file faulty, cant get previos sibling.");
    }
    else
    {    
        auto last_number_node = last_child->get_first_child("number")->get_first_child();
        auto last_number_text = dynamic_cast<const xmlpp::TextNode*>(last_number_node);

        if(!last_number_text)
        {
            logger::log_warning("Month file : could not get last number text.");
        }
        else
        {       
            int new_number = stoi(last_number_text->get_content()) + 1;

            auto number_node = new_child->add_child_element("number");
            number_node->add_child_text(to_string(new_number));

            auto date_node = new_child->add_child_element("date");
            date_node->add_child_text(date);

            auto reason_node = new_child->add_child_element("reason");
            reason_node->add_child_text(reason);

            auto ammount_node = new_child->add_child_element("ammount");
            ammount_node->add_child_text(ammount);

            doc->write_to_file(save_file);

            logger::log_info("Added entry.");
        }
    }
}

void save::set_budget(std::string new_budget)
{
    xmlpp::DomParser parser;
    parser.parse_file(vars_file);
    auto doc = parser.get_document();

    auto root_node = doc->get_root_node();
    auto budget_node = root_node->get_first_child("budget");
    auto month_node = budget_node->get_first_child(this_month);
    auto number_node = month_node->get_first_child();

    auto budget_text = dynamic_cast<xmlpp::TextNode*>(number_node);

    if (budget_text)
    {   
        budget_text->set_content(new_budget);
        doc->write_to_file(vars_file);
        logger::log_info("Changed budget.");
    }
    else
    {
        logger::log_warning("Set Budget : could not set budget node to text.");
    }
}

void save::remove_entry(std::string entry_number)
{
    xmlpp::DomParser parser;
    parser.parse_file(save_file);
    auto doc = parser.get_document();

    auto root_element = doc->get_root_node();
    auto list_children = root_element->get_children();

    for (auto const& i : list_children)
    {   
        auto number_node = i->get_first_child("number")->get_first_child();
        auto number_text = dynamic_cast<const xmlpp::TextNode*>(number_node);

        if(number_text)
        {

            if (stoi(entry_number) == stoi(number_text->get_content()))
            {
                root_element->remove_node(i);
                doc->write_to_file(save_file);
                update_numbers();
                logger::log_info("Removed entry");
            }
        }
        else
        {
            logger::log_warning("Remove Entry : could not typecast the number node.");
        }
        
    }
}

void save::update_numbers()
{
    xmlpp::DomParser parser;
    parser.parse_file(save_file);
    auto doc = parser.get_document();

    auto root_element = doc->get_root_node();
    auto list_children = root_element->get_children();

    int number = 0;

    for (auto const& i : list_children)
    {   
        auto number_node = i->get_first_child("number")->get_first_child();
        auto number_text = dynamic_cast< xmlpp::TextNode*>(number_node);

        if(number_text)
        {
            number_text->set_content(std::to_string(number));

            logger::log_debug("Updated the entry numbers.");
        }
        else
        {
            logger::log_warning("Update Numbers : failed to typecast number node");
        }
        
        number ++;
    }

    doc->write_to_file(save_file);

    parser.parse_file(month_file);
    doc = parser.get_document();

    root_element = doc->get_root_node();
    auto month_node = dynamic_cast<xmlpp::Element*>(root_element->get_first_child(this_month));

    if(month_node)
    {
        auto list_children = month_node->get_children();

        int number = 0;

        for (auto const& i : list_children)
        {   
            auto number_node = i->get_first_child("number")->get_first_child();
            auto number_text = dynamic_cast< xmlpp::TextNode*>(number_node);

            if(number_text)
            {
                number_text->set_content(std::to_string(number));

                logger::log_debug("Updated the monthly numbers.");
            }
            else
            {
                logger::log_warning("Update Numbers : failed to typecast number node");
            }
        
            number ++;
        }

        doc->write_to_file(month_file);
    }
}

void save::edit_ammount(std::string entry_number, std::string new_ammount)
{
    xmlpp::DomParser parser;
    parser.parse_file(save_file);
    auto doc = parser.get_document();

    auto root_element = doc->get_root_node();
    auto list_children = root_element->get_children();

    int highest_entry_number = 1;

    for (auto const& i : list_children)
    {   
        auto number_node = i->get_first_child("number")->get_first_child();
        auto number_text = dynamic_cast<const xmlpp::TextNode*>(number_node);

        if(number_text)
        {
            highest_entry_number++;

            if (stoi(entry_number) == stoi(number_text->get_content()))
            {
                auto ammount_node = i->get_first_child("ammount")->get_first_child();
                auto ammount_text = dynamic_cast<xmlpp::TextNode*>(ammount_node);
                if (ammount_text)
                {
                    ammount_text->set_content(new_ammount);
                    doc->write_to_file(save_file);

                    logger::log_info("Edited an amount.");
                }
                else
                {
                    logger::log_debug("Edit amount : failed to typecast amount node.");
                }
                
            }
        }
        else
        {
            logger::log_warning("Edit amount : failed to typecast number node.");
        }
        
    }
}

void save::edit_reason(std::string entry_number, std::string new_reason)
{
    xmlpp::DomParser parser;
    parser.parse_file(save_file);
    auto doc = parser.get_document();

    auto root_element = doc->get_root_node();
    auto list_children = root_element->get_children();

    int highest_entry_number = 1;

    for (auto const& i : list_children)
    {   
        auto number_node = i->get_first_child("number")->get_first_child();
        auto number_text = dynamic_cast<const xmlpp::TextNode*>(number_node);

        if(number_text)
        {
            highest_entry_number++;

            if (stoi(entry_number) == stoi(number_text->get_content()))
            {
                auto reason_node = i->get_first_child("reason")->get_first_child();
                auto reason_text = dynamic_cast<xmlpp::TextNode*>(reason_node);
                if (reason_text)
                {
                    reason_text->set_content(new_reason);
                    doc->write_to_file(save_file);

                    logger::log_info("Edited a reason.");
                }
                else
                {
                    logger::log_debug("Edit reason : failed to typecast reason node.");
                }
            }
        }
        else
        {
            logger::log_warning("Edit reason : failed to typecast number node.");
        }
        
    }
}

void save::add_monthly(std::string amount, std::string reason)
{
    xmlpp::DomParser parser;
    parser.parse_file(month_file);
    auto doc = parser.get_document();

    auto root_element = doc->get_root_node();
    auto month_node = dynamic_cast<xmlpp::Element*>(root_element->get_first_child(this_month));

    if(month_node)
    {
        auto new_child = month_node->add_child_element("entry");
        auto last_child = new_child->get_previous_sibling();

        if(last_child)
        {
            auto last_number_node = last_child->get_first_child("number")->get_first_child();
            auto last_number_text = dynamic_cast<const xmlpp::TextNode*>(last_number_node);
            
            if (last_number_text)
            {
                int new_number = stoi(last_number_text->get_content()) + 1;

                auto number_node = new_child->add_child_element("number");
                number_node->add_child_text(to_string(new_number));

                auto reason_node = new_child->add_child_element("reason");
                reason_node->add_child_text(reason);

                auto ammount_node = new_child->add_child_element("ammount");
                ammount_node->add_child_text(amount);

                doc->write_to_file(month_file);

                logger::log_info("Added monthly.");
            }
            else
            {
                logger::log_warning("Add monthly : failed to typecast number node.");
            }
        }
        else
        {
            logger::log_warning("Add monthly : failed to get prevois entry sibling");
        }        
    }
    else
    {
        logger::log_warning("Add monthly : failed to get month node.");
    }    
}

void save::remove_monthly(std::string entry_number)
{
    xmlpp::DomParser parser;
    parser.parse_file(month_file);
    auto doc = parser.get_document();

    auto root_element = doc->get_root_node();
    auto month_node = dynamic_cast<xmlpp::Element*>(root_element->get_first_child(this_month));

    if(month_node)
    {
        auto list_children = month_node->get_children();

        for (auto const& i : list_children)
        {   
            auto number_node = i->get_first_child("number")->get_first_child();
            auto number_text = dynamic_cast<const xmlpp::TextNode*>(number_node);

            if(number_text)
            {

                if (stoi(entry_number) == stoi(number_text->get_content()))
                {
                    month_node->remove_node(i);
                    doc->write_to_file(month_file);
                    update_numbers();
                    logger::log_info("Removed monthly");
                }
            }
            else
            {
                logger::log_warning("Remove monthly : could not typecast the number node.");
            }
        }
    }
    else
    {
        logger::log_warning("Remove monthly : failed to typecast month node.");
    }
    
}


std::string reader::get_string_from_xml(std::string file)
{
    xmlpp::DomParser parser;
    parser.parse_file(file);
    auto doc = parser.get_document();

    return doc->write_to_string();
}

std::string reader::get_budget()
{
    logger::log_debug("Trying to get budget.");

    xmlpp::DomParser parser;
    parser.parse_file(vars_file);
    auto doc = parser.get_document();

    auto root_node = doc->get_root_node();
    auto budget_node = root_node->get_first_child("budget");
    auto month_node = budget_node->get_first_child(this_month);
    auto number_node = month_node->get_first_child();

    auto budget_text = dynamic_cast<const xmlpp::TextNode*>(number_node);

    if (budget_text)
    {
        return budget_text->get_content();
    }
    else
    {
        logger::log_warning("Get budget : failed to typecast budget text.");
    }
    

    return "0";
}

int reader::get_last_entry_number()
{
    xmlpp::DomParser parser;
    parser.parse_file(save_file);
    auto doc = parser.get_document();

    auto root_element = doc->get_root_node();
    auto list_children = root_element->get_children();

    return list_children.size();
}

string reader::read_ammount(string entry_number)
{
    xmlpp::DomParser parser;
    parser.parse_file(save_file);
    auto doc = parser.get_document();

    auto root_element = doc->get_root_node();
    auto list_children = root_element->get_children();

    int highest_entry_number = 1;

    for (auto const& i : list_children)
    {   
        auto number_node = i->get_first_child("number")->get_first_child();
        auto number_text = dynamic_cast<const xmlpp::TextNode*>(number_node);

        if(number_text)
        {
            highest_entry_number++;

            if (stoi(entry_number) == stoi(number_text->get_content()))
            {
                auto ammount_node = i->get_first_child("ammount")->get_first_child();
                auto ammount_text = dynamic_cast<const xmlpp::TextNode*>(ammount_node);
                if (ammount_text)
                {
                    return ammount_text->get_content();
                }
            }
        }
        else
        {
            logger::log_warning("Read amount : failed to typecast number node.");
        }
        
    }
    
    return "Not Found";
}

string reader::read_reason(string entry_number)
{
    xmlpp::DomParser parser;
    parser.parse_file(save_file);
    auto doc = parser.get_document();

    auto root_element = doc->get_root_node();
    auto list_children = root_element->get_children();

    for (auto const& i : list_children)
    {   
        auto number_node = i->get_first_child("number")->get_first_child();
        auto number_text = dynamic_cast<const xmlpp::TextNode*>(number_node);

        if(number_text)
        {
            if (stoi(entry_number) == stoi(number_text->get_content()))
            {
                auto reason_node = i->get_first_child("reason")->get_first_child();
                auto reason_text = dynamic_cast<const xmlpp::TextNode*>(reason_node);
                if (reason_text)
                {
                    return reason_text->get_content();
                }
            }
        }
        else
        {
            logger::log_warning("Read reason : failed to typecast the number node.");
        }
        
    }


    return "Not Found";
}

std::string reader::read_date(std::string entry_number)
{
    xmlpp::DomParser parser;
    parser.parse_file(save_file);
    auto doc = parser.get_document();

    auto root_element = doc->get_root_node();
    auto list_children = root_element->get_children();

    for (auto const& i : list_children)
    {   
        auto number_node = i->get_first_child("number")->get_first_child();
        auto number_text = dynamic_cast<const xmlpp::TextNode*>(number_node);

        if(number_text)
        {
            if (stoi(entry_number) == stoi(number_text->get_content()))
            {
                auto date_node = i->get_first_child("date")->get_first_child();
                auto date_text = dynamic_cast<const xmlpp::TextNode*>(date_node);
                if (date_text)
                {
                    return date_text->get_content();
                }
                else
                {
                    logger::log_warning("Read date : failed to typecast the date node.");
                }
                
            }
        }
        else
        {
            logger::log_warning("Read date : failed to typecast the number node.");
        }
    }

    return "Error";
}

std::list<std::vector<std::string>> reader::get_every_monthly()
{
    std::list<std::vector<std::string>> every_monthly;

    xmlpp::DomParser parser;
    parser.parse_file(month_file);
    auto doc = parser.get_document();

    auto root_element = doc->get_root_node();
    auto month_node = dynamic_cast<xmlpp::Element*>(root_element->get_first_child(this_month));

    if(month_node)
    {
        auto monthly_children = month_node->get_children();

        for(auto const& i : monthly_children)
        {
            auto reason_node = i->get_first_child("reason");
            auto amount_node = i->get_first_child("ammount");
            auto number_node = i->get_first_child("number");
            
            if(reason_node && amount_node)
            {
                auto reason_text_node = reason_node->get_first_child();
                auto ammount_text_node = amount_node->get_first_child();
                auto number_text_node = number_node->get_first_child();

                if (reason_text_node && ammount_text_node)
                {
                    auto reason_text = dynamic_cast<const xmlpp::TextNode*>(reason_text_node);
                    auto amount_text = dynamic_cast<const xmlpp::TextNode*>(ammount_text_node);
                    auto number_text = dynamic_cast<const xmlpp::TextNode*>(number_text_node);

                    if (amount_text && reason_text)
                    {
                        std::vector<std::string> monthly;

                        std::string reason = reason_text->get_content();
                        std::string amount = amount_text->get_content();
                        std::string number = number_text->get_content();

                        if(!reason.empty() && !amount.empty() && flib::is_digit(amount))
                        {
                            std::vector<std::string> entry;
                            entry.push_back(amount);
                            entry.push_back(reason);
                            entry.push_back(number);

                            every_monthly.push_back(entry);
                        }
                    }
                }
            }
        }
    }
    else
    {
        logger::log_warning("Get every month : failed to typecast month node.");
    }

    return every_monthly;
}


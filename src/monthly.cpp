#include "monthly.h"
#include "lib/logger/logger.h"
#include "config/monthly_config.h"

monthly::monthly()
{
    update();

    append_column("Amonnt", m_monthly_columns.m_col_ammount);
    append_column("Reason", m_monthly_columns.m_col_reason);

    init_pop_up();
}

monthly::type_signal_budget monthly::signal_budget()
{
  return m_signal_budget;
}

void monthly::update_budget()
{
  m_signal_budget.emit();
}

void monthly::init_pop_up()
{
  std::string infile = POP_UP_CONFIG_MONTHLY;
  const char* ui_info = infile.c_str();

  m_refBuilder->add_from_string(ui_info);
  Glib::RefPtr<Glib::Object> object = m_refBuilder->get_object("entry_pop_up");
  Glib::RefPtr<Gio::Menu> gmenu = Glib::RefPtr<Gio::Menu>::cast_dynamic(object);

  m_pop_up_menu = new Gtk::Menu(gmenu);

  m_refActionGroup = Gio::SimpleActionGroup::create();
  m_remove_action = m_refActionGroup->add_action("remove", sigc::mem_fun(*this, &monthly::remove));

  insert_action_group("entry_pop_up", m_refActionGroup);
}

void monthly::remove()
{
  auto select = get_selection();
  auto row_iter = select->get_selected();

  if (row_iter)
  {
    auto number = row_iter->get_value(m_monthly_columns.m_col_number);
    m_refTreeModel->erase(row_iter);
    set_model(m_refTreeModel);

    m_saver.remove_monthly(number);

    redraw();

    return;
  }

  return;
}

void monthly::update()
{
    m_refTreeModel = Gtk::ListStore::create(m_monthly_columns);
    set_model(m_refTreeModel);

    auto every_monthly = m_reader.get_every_monthly();

    for (auto entry = every_monthly.begin(); entry != every_monthly.end(); ++entry)
    {
        Gtk::TreeModel::Row row = *(m_refTreeModel->append());
        row[m_monthly_columns.m_col_ammount] = (*entry)[0];
        row[m_monthly_columns.m_col_reason] = (*entry)[1];
        row[m_monthly_columns.m_col_number] = (*entry)[2];
        
    }
}

void monthly::redraw()
{
    update_budget();

    update();

    queue_draw();
}

bool monthly::on_button_press_event(GdkEventButton* button_event)
{
  if (button_event->button == 3)
  {   
      button_event->button = 1;
      Gtk::TreeView::on_button_press_event(button_event);
      button_event->button = 3;
      if(!m_pop_up_menu->get_attach_widget())
          m_pop_up_menu->attach_to_widget(*this);

      m_pop_up_menu->popup(button_event->button, button_event->time);
      return true;
  }

  return Gtk::TreeView::on_button_press_event(button_event);
}
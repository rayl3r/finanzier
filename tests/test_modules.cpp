#include "save_read.h"
#include "initialisation.h"
#include "string"
#include "calculator.h"
#include <stdio.h>
#include <ctime>


bool cleanup(std::string save_folder)
{
    initialisation m_init;

    std::string year = m_init.get_year();
    std::string month = m_init.get_month();

    if (remove((save_folder + "/" + year + "/" + month + ".xml").c_str()) != -1 && remove((save_folder + "/" + year + "/vars.xml").c_str()) != -1 && remove((save_folder + "/" + year + "/monthly.xml").c_str()) != -1)
    {
        if(rmdir((save_folder + "/" + year).c_str()) != -1)
        {
            if(rmdir(save_folder.c_str()) != -1)
            {
                return true;
            }
        }
    }

    return false;
}

int main()
{   
    initialisation m_ini;
    reader m_reader;
    save m_saver;
    calculator m_calc;

    time_t now = time(0);    
    tm *dt = localtime(&now);    
    int this_day = dt->tm_mday;
    int this_year = 1900 + dt->tm_year;
    int i_this_month = 1 + dt->tm_mon;

    std::string save_path = "test_path";
    std::string date = std::to_string(this_day) + "." + std::to_string(i_this_month) + "." + std::to_string(this_year);

    try
    {
        m_ini.set_save_folder(save_path);
        m_reader.set_save_folder(save_path);
        m_saver.set_save_folder(save_path);
        m_calc.change_save_folder(save_path);
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        return 1;
    }

    try
    {
        m_ini.run();
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        return 1;
    }

    try
    {
        m_saver.add_entry("12", "Essen");
        m_saver.add_entry("14", "Schuhe");
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        return 1;
    }
    
    try
    {
        m_saver.set_budget("40");
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        return 1;
    }

    if(!(m_reader.get_last_entry_number() == 3))
    {
        std::cout <<  "failed to get lenght of entrys" << std::endl;
        return m_reader.get_last_entry_number();
    }

    if(strcmp(m_reader.get_budget().c_str() , "40") != 0)
    {
        std::cout <<  "failed to read the budget" << std::endl;
        return 1;
    }

    if(strcmp(m_reader.read_ammount("1").c_str() , "12") != 0 || strcmp(m_reader.read_ammount("2").c_str() , "14") != 0)
    {
        std::cout <<  "failed to read ammount" << std::endl;
        return 1;
    }

    if(strcmp(m_reader.read_reason("1").c_str() , "Essen") != 0 || strcmp(m_reader.read_reason("2").c_str() , "Schuhe") != 0)
    {
        std::cout <<  "failed to read reason" << std::endl;
        return 1;
    }

    if(strcmp(m_reader.read_date("1").c_str() , date.c_str()) != 0 || strcmp(m_reader.read_reason("2").c_str() , "Schuhe") != 0)
    {
        std::cout <<  "failed to read date" << std::endl;
        return 1;
    }

    try
    {
        m_saver.remove_entry("2");
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        return 1;
    }
    if(!(m_reader.get_last_entry_number() == 2))
    {
        std::cout <<  "failed to remove entrys" << std::endl;
        return m_reader.get_last_entry_number();
    }    

    if(!(m_calc.sum_up_ammounts() == 12))
    {
        std::cout << "failed calculator" << std::endl;
        std::cout << m_calc.sum_up_ammounts() << std::endl;
        return 1;
    }

    if (!cleanup(save_path))
    {
        std::cout <<  "failed to clean up" << std::endl;
        return 1;
    }

    return 0;
}
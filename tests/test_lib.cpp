#include "lib/finanzier/finanzierlib.h"
#include <string>
#include <iostream>

int main()
{
    std::string test_string = "10.12";
    std::string test_string_2 = "12.1a";
    if(!flib::is_digit(test_string) && flib::is_digit(test_string_2))
    {
        std::cout<< "Is Digit failed" << std::endl;
        return 1;
    }

    test_string = "12";
    if(!(flib::convert_to_int_times_hundret(test_string)==1200))
    {
        std::cout<< "Is convert int times hundret failed" << std::endl;
        return 1;
    }

    test_string = "12.";
    if(!(flib::convert_to_int_times_hundret(test_string)==1200))
    {
        std::cout<< "Is convert int times hundret failed" << std::endl;
        return 1;
    }

    test_string = "12.12";
    if(!(flib::convert_to_int_times_hundret(test_string)==1212))
    {
        std::cout<< "Is convert int times hundret failed" << std::endl;
        return 1;
    }

    test_string = "12.1";
    if(!(flib::convert_to_int_times_hundret(test_string)==1210))
    {
        std::cout<< "Is convert int times hundret failed" << std::endl;
        return 1;
    }

    test_string = "12,";
    if(!(flib::convert_to_int_times_hundret(test_string)==1200))
    {
        std::cout<< "Is convert int times hundret failed" << std::endl;
        return 1;
    }

    test_string = "12,12";
    if(!(flib::convert_to_int_times_hundret(test_string)==1212))
    {
        std::cout<< "Is convert int times hundret failed" << std::endl;
        return 1;
    }

    test_string = "12,1";
    if(!(flib::convert_to_int_times_hundret(test_string)==1210))
    {
        std::cout<< "Is convert int times hundret failed" << std::endl;
        return 1;
    }

    test_string = "11.2123123123123123";
    test_string_2 = "11,33312312312312";
    if(!((flib::cut_down(test_string).length() == 5) && (flib::cut_down(test_string_2).length() == 5)))
    {
        std::cout<< "Cut down failed : " <<  flib::cut_down(test_string) << "  " <<  flib::cut_down(test_string_2) << std::endl;
        return 1;
    }

    return 0;
}
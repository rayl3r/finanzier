# finanzier
An app to track your finances.

[![pipeline status](https://gitlab.com/rayl3r/finanzier/badges/master/pipeline.svg)](https://gitlab.com/rayl3r/finanzier/commits/master)
[![coverage report](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/coverage.svg?job=coverage)](https://gitlab.com/veloren/veloren/commits/master)
## Dependencies
The most maschines have these dependencies pre installed:
- gcc
- clang
- libecpg

But better check.

Other than that, these are dependencies you propably have to install:

- [Meson](https://mesonbuild.com/Getting-meson.html "Download Meson")

- [gtkmm](https://www.gtkmm.org/en/download.html "Download gtkmm")

- [libxml++](https://developer.gnome.org/libxml++-tutorial/stable/ "libxml++")

## Building
### Under Linux

1. Install the dependencies
2. Clone the Repository
3. Open a Terminal in the repo folder and run the commands 
    - `meson builddir`
    - `ninja -C builddir`
5. The Application is now in your builddir folder and can be run

### Under Windows
Propably the best way is to install a porgramm like [MSYS2](https://www.msys2.org/ "MYSYS2 Homepage") and the follow the Linux steps.

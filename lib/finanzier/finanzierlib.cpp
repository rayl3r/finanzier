#include "lib/finanzier/finanzierlib.h"

#include <sstream>
#include <fstream>
#include <iostream>

int flib::conv_char_int(const char* value)
{
    std::stringstream strValue;
    strValue << value;

    unsigned int intValue;
    strValue >> intValue;

    return intValue;
}

bool flib::file_exists(const char* filename)
{
    std::ifstream ifile(filename);
    return (bool)ifile;
}

std::string flib::switch_month(int month)
{
    switch (month)
    {
    case 1:
        return "January";
        break;
    case 2:
        return "February";
        break;
    case 3:
        return "March";
        break;
    case 4:
        return "April";
        break;
    case 5:
        return "May";
        break;
    case 6:
        return "June";
        break;
    case 7:
        return "July";
        break;
    case 8:
        return "August";
        break;
    case 9:
        return "September";
        break;
    case 10:
        return "October";
        break;
    case 11:
        return "November";
        break;
    case 12:
        return "December";
        break;
    
    default:
        return "no Month";
        break;
    }
}

std::string flib::get_node_string(const xmlpp::Node* node)
{
    //std::cout << std::endl; //Separate nodes by an empty line.

    const auto nodeContent = dynamic_cast<const xmlpp::ContentNode*>(node);
    const auto nodeText = dynamic_cast<const xmlpp::TextNode*>(node);
    const auto nodeComment = dynamic_cast<const xmlpp::CommentNode*>(node);

    if(nodeText && nodeText->is_white_space()) //Let's ignore the indenting - you don't always want to do this.
      return "white_space";

    const auto nodename = node->get_name();


    if(!nodeText && !nodeComment && !nodename.empty()) //Let's not say "name: text".
    {
      const auto namespace_prefix = node->get_namespace_prefix();

      //std::cout << "Node name = ";
      
      //std::cout << namespace_prefix << ":";
      //std::cout << nodename << std::endl;

      return nodename;
      
    }

    if(nodeText)
    {
      return nodeText->get_content();
      //std::cout  << "text = \"" << nodeText->get_content() << "\"" << std::endl;
    }
    else if(nodeComment)
    {
      return nodeComment->get_content();
      //std::cout  << "comment = " << nodeComment->get_content() << std::endl;
    }
    else if(nodeContent)
    {
      return nodeContent->get_content();
      //std::cout << "content = " << nodeContent->get_content() << std::endl;
    }
    else if(const xmlpp::Element* nodeElement = dynamic_cast<const xmlpp::Element*>(node))
    {
      //A normal Element node:

      //line() works only for ElementNodes.
      //std::cout  << "     line = " << node->get_line() << std::endl;

      //Print attributes:
      for (const auto& attribute : nodeElement->get_attributes())
      {
        const auto namespace_prefix = attribute->get_namespace_prefix();

        //std::cout  << "  Attribute ";
        //if(!namespace_prefix.empty())
          //std::cout << namespace_prefix << ":";
        //std::cout << attribute->get_name() << " = "
        //        << attribute->get_value() << std::endl;
        return attribute->get_name();
      }
    }
    else
    {
        throw std::invalid_argument("No Node");
    }
    return "Error";
}


bool flib::is_digit(const std::string &str)
{
    return str.find_first_not_of("0123456789.,") == std::string::npos;
}

int flib::convert_to_int_times_hundret(const std::string str)
{
    std::string my_string = str;

    if(my_string.find_first_not_of("0123456789") == std::string::npos)
    {
        return std::stoi(my_string) * 100;
    }
    else if (my_string.find_first_not_of("0123456789.") == std::string::npos)
    {
        auto pos = my_string.find(".");

        int mul = 1;
        if((my_string.length() - pos) == 2)
        {
            mul = 10;
        }
        if((my_string.length() - pos) == 1)
        {
            mul = 100;
        }

        my_string.erase(pos,1);

        return std::stoi(my_string) * mul;
    }
    else if(my_string.find_first_not_of("0123456789,") == std::string::npos)
    {
        auto pos = my_string.find(",");

        int mul = 1;
        if((my_string.length() - pos) == 2)
        {
            mul = 10;
        }
        if((my_string.length() - pos) == 1)
        {
            mul = 100;
        }

        my_string.erase(pos,1);

        return std::stoi(my_string) * mul;
    }
    return 0;
}

std::string flib::cut_down(std::string str)
{
    std::string my_string = str;

    if(my_string.find_first_not_of("0123456789") == std::string::npos)
    {
        return "0";
    }
    else if(my_string.find_first_not_of("0123456789,") == std::string::npos)
    {
        auto pos = my_string.find(",");

        if((my_string.length() - pos) > 2)
        {
            my_string.erase(pos+3,my_string.length());
        }

        return my_string;

    }
    else if (my_string.find_first_not_of("0123456789.") == std::string::npos)
    {
        auto pos = my_string.find(".");

        if((my_string.length() - pos) > 2)
        {
            my_string.erase(pos+3,my_string.length());
        }

        return my_string;
    }

    return "0";
}

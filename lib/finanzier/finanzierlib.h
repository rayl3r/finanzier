#pragma once

#include <stdio.h>
#include <string>
#include <libxml++/libxml++.h>

class flib
{
    public:
    flib(){};
    virtual ~flib(){};

    virtual void m_flib() = 0;

    /*Converts a char into an Integer.*/
    static int conv_char_int(const char* value);

    /*Checks if a file exists from root folder.*/
    static bool file_exists(const char* filename);

    /*Gets the String of an xmlpp Node.*/
    static std::string get_node_string(const xmlpp::Node* node);

    /*Returns a Month out of a number between 1 and 12.*/
    static std::string switch_month(int month);

    /*Tests if an string is a digit.*/
    static bool is_digit(const std::string &str);

    /*Makes out of an decimal string a Integer times hundret.*/
    static int convert_to_int_times_hundret(const std::string);

    /*Cuts a decimal string array down to two numbers after the point or comma.*/
    static std::string cut_down(std::string);
};
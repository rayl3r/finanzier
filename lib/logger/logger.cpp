#include "logger.h"

using namespace std;

void logger::log_error(std::string to_log)
{
    std::string save_folder = SAVE_PATH;
    time_t rawtime;
    time (&rawtime);
    ofstream out_file;
    out_file.open(save_folder + "/finanzier.log", std::ios_base::app);

    out_file << "[ERROR :] Time ->  " << ctime(&rawtime) << " \t Message -->  '"<< to_log << "'\n" << endl;

    out_file.close();
}

void logger::log_debug(std::string to_log)
{
    if (LOG_LEVEL < 1)
    {
        std::string save_folder = SAVE_PATH;
        time_t rawtime;
        time (&rawtime);
        ofstream out_file;
        out_file.open(save_folder +"/finanzier.log", std::ios_base::app);
    
        out_file << "[DEBUG :] Time ->  " << ctime(&rawtime) << " \t Message -->  '"<< to_log << "'\n" << endl;

        out_file.close();
    }
}

void logger::log_warning(std::string to_log)
{
    if(LOG_LEVEL < 3)
    {
        std::string save_folder = SAVE_PATH;
        time_t rawtime;
        time (&rawtime);
        ofstream out_file;
        out_file.open(save_folder +"/finanzier.log", std::ios_base::app);
    
        out_file << "[WARNING :] Time ->  " << ctime(&rawtime) << " \t Message -->  '"<< to_log << "'\n" << endl;

        out_file.close();
    }
}

void logger::log_info(std::string to_log)
{
    if(LOG_LEVEL < 2)
    {
        std::string save_folder = SAVE_PATH;
        time_t rawtime;
        time (&rawtime);
        ofstream out_file;
        out_file.open(save_folder +"/finanzier.log", std::ios_base::app);
    
        out_file << "[INFO :] Time ->  " << ctime(&rawtime) << " \t Message -->  '"<< to_log << "'\n" << endl;

        out_file.close();
    }
}
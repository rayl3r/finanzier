#pragma once
#include <string>
#include <fstream>
#include <time.h>
#include "config.h"

class logger
{
    public:
    logger(){};
    virtual ~logger(){};

    static void log_error(std::string to_log);
    static void log_debug(std::string to_log);
    static void log_warning(std::string to_log);
    static void log_info(std::string to_log);
    
    virtual void logging() = 0;
};
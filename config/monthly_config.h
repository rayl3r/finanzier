#include <string>

std::string POP_UP_CONFIG_MONTHLY = 
"<interface>"
    "<menu id='entry_pop_up'>"
      "<section>"
        "<item>"
         "<attribute name='label' translatable='yes'>Remove</attribute>"
          "<attribute name='action'>entry_pop_up.remove</attribute>"
        "</item>"
     "</section>"
    "</menu>"
 "</interface>";
# Get the base fedora image from Docker Hub
FROM fedora:latest
 
# Update apps on the base image
RUN dnf -y update
 
# Install the dependencies
RUN dnf -y install meson gtkmm30-devel libxml++30-devel gcc clang libecpg git
#pragma once

#include "gtkmm.h"
#include "table.h"
#include <string>
#include "save_read.h"
#include "dialogs.h"
#include "lib/finanzier/finanzierlib.h"
#include "config.h"
#include "monthly.h"
#include "budget_area.h"

class gui : public Gtk::Window
{
    public:
    gui();
    virtual ~gui(){};

    void update_budget();
    table m_table;
    monthly m_monthy_table;

    private:

    budget_dialog enter_budget;
    monthly_dialog m_monthly_dialog;
    calculator m_calculator;
    budget_area m_budget_area;


    protected:

    void add_to_entrys();
    void close();
    void show_budget_dialog();
    void show_monthly_dialog();

    void resize_all();

    //init functions
    void init_buttons_labels();
    void init_buttons_signal();
    void init_left_side_bar();
    void init_Entry_boxes();
    void init_middle_bar();
    void init_main_box();
    void init_scrolled_window();

    save m_saver;
    reader m_reader;
    Gtk::Box main_box;
    Gtk::Button add_Entry, change_budget, add_monthly_button;
    Gtk::ScrolledWindow m_ScrolledWindow;
    Gtk::ScrolledWindow m_monthlyScrolledWindow;
    Gtk::Box Entry_boxes, left_side_bar , middle_bar;
    Gtk::Entry ammount_Entry, reason_Entry;
    Gtk::Label budget, summed_budget, budget_text, plus_sign, expenses_label;

    //little boxes
    Gtk::Label ammount_label, reason_label;
    Gtk::VBox ammount_box, reson_box;
    Gtk::VBox budget_box;
    Gtk::HBox sum_box;

    bool on_key_press_event(GdkEventKey* event) override;
};


#pragma once
#include "gtkmm.h"
#include "save_read.h"
#include <string>
#include "list"
#include <iostream>
#include <sigc++/sigc++.h>

class table_colomn : public Gtk::TreeModel::ColumnRecord
{
    public:

    table_colomn(){ add(m_col_date); add(m_col_reason); add(m_col_ammount); add(m_col_number);};
    virtual ~table_colomn(){};

    Gtk::TreeModelColumn<std::string> m_col_date;
    Gtk::TreeModelColumn<std::string> m_col_reason;
    Gtk::TreeModelColumn<std::string> m_col_ammount;
    Gtk::TreeModelColumn<std::string> m_col_number;
};

class table : public Gtk::TreeView
{
    public:

    table();
    ~table(){};

    Glib::RefPtr<Gtk::ListStore> m_refTreeModel;
    table_colomn m_table_colomns;

    void redraw();
    void remove_entry();

    typedef sigc::signal<void> type_signal_budget;
    type_signal_budget signal_budget();
    type_signal_budget m_signal_budget;

    void update_budget();

    protected:

    bool on_button_press_event(GdkEventButton* button_event) override;

    void treeviewcolumn_ammount_on_cell_data(Gtk::CellRenderer* renderer, const Gtk::TreeModel::iterator& iter);
    void cellrenderer_ammount_on_editing_started(Gtk::CellEditable* cell_editable, const Glib::ustring& path);
    void cellrenderer_ammount_on_edited(const Glib::ustring& path_string, const Glib::ustring& new_text);

    Gtk::CellRendererText m_cellrenderer_ammount;
    Gtk::TreeView::Column m_treeviewcolumn_ammount;
    bool m_validate_retry;
    Glib::ustring m_invalid_text_for_retry;

    void treeviewcolumn_reason_on_cell_data(Gtk::CellRenderer* renderer, const Gtk::TreeModel::iterator& iter);
    void cellrenderer_reason_on_editing_started(Gtk::CellEditable* cell_editable, const Glib::ustring& path);
    void cellrenderer_reason_on_edited(const Glib::ustring& path_string, const Glib::ustring& new_text);

    Gtk::CellRendererText m_cellrenderer_reason;
    Gtk::TreeView::Column m_treeviewcolumn_reason;

    private:

      void get_every_entry();
      void draw_every_colomn();
      void init_pop_up();
      void init_actions();
      void init_columns();

      Glib::RefPtr<Gtk::Builder> m_refBuilder = Gtk::Builder::create();
      Glib::RefPtr<Gio::SimpleAction> m_remove_action;
      Glib::RefPtr<Gio::SimpleActionGroup> m_refActionGroup;
      Gtk::Menu* m_pop_up_menu;

      reader m_reader;
      save m_saver;
      std::list<std::vector<std::string>> list_entrys;
      //0 is ammount,
      //1 is reason,
      //2 is date
};
#pragma once
#include "gtkmm/drawingarea.h"
#include <string>
#include "calculator.h"
#include "lib/finanzier/finanzierlib.h"

class budget_area : public Gtk::DrawingArea
{
    public:
    budget_area();
    ~budget_area(){};

    void redraw();

    bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;

    private:

    calculator m_calc;


    int budget;
    float summed;

    void update();
    void draw_text(const Cairo::RefPtr<Cairo::Context>& cr, int pos_width, int pos_height, std::string text);
};
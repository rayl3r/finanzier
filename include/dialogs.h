#pragma once
#include "gtkmm.h"
#include "save_read.h"
#include "lib/finanzier/finanzierlib.h"

class budget_dialog : public Gtk::Dialog
{
    public:
    budget_dialog();
    ~budget_dialog(){};

    private:

    Gtk::HBox button_box;
    Gtk::Button cancel_button, ok_button;
    Gtk::Entry budget_entry;

    save saver;

    void init_buttons();
    void close();
    void change_budget();

    bool on_key_press_event(GdkEventKey* event) override;
};

class monthly_dialog : public Gtk::Dialog
{
    public:
    monthly_dialog();
    ~monthly_dialog(){};

    private:
    
    Gtk::HBox button_box, entry_box;    
    Gtk::Button cancel_button, add_button;
    Gtk::Entry amount_entry, reason_entry;

    save m_saver;

    void init();
    void close();
    void add_monthly();

    bool on_key_press_event(GdkEventKey* event) override;
};
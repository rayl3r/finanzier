#pragma once
#include "save_read.h"
#include <list>
#include <string>
#include "lib/finanzier/finanzierlib.h"

class calculator
{
    public:
    calculator(){};
    ~calculator(){};

    float sum_up_ammounts();
    void change_save_folder(std::string);
    int get_budget();

    private:
    
    void get_all_ammounts();
    std::list<float> list_ammounts;
    reader m_reader;

};
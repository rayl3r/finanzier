#pragma once
#include "gtkmm.h"
#include "save_read.h"

class monthly_columns : public Gtk::TreeModelColumnRecord
{   
    public:
    monthly_columns(){add(m_col_ammount); add(m_col_reason); add(m_col_number);};
    ~monthly_columns(){};

    Gtk::TreeModelColumn<std::string> m_col_ammount;
    Gtk::TreeModelColumn<std::string> m_col_reason;
    Gtk::TreeModelColumn<std::string> m_col_number;
};

class monthly : public Gtk::TreeView
{
    public:
    monthly();
    ~monthly(){};

    void redraw();

    typedef sigc::signal<void> type_signal_budget;
    type_signal_budget signal_budget();
    type_signal_budget m_signal_budget;
    void update_budget();

    protected:

    bool on_button_press_event(GdkEventButton* button_event) override;

    private:

    reader m_reader;
    save m_saver;

    Glib::RefPtr<Gtk::ListStore> m_refTreeModel;
    monthly_columns m_monthly_columns;

    Gtk::Menu* m_pop_up_menu;
    Glib::RefPtr<Gtk::Builder> m_refBuilder = Gtk::Builder::create();
    Glib::RefPtr<Gio::SimpleAction> m_remove_action;
    Glib::RefPtr<Gio::SimpleActionGroup> m_refActionGroup;

    void update();
    void init_pop_up();
    void remove();
};
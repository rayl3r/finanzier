#pragma once
#include <stdio.h>
#include <iostream>
#include <string>
#include <sys/stat.h>
#include <fstream>
#include <unistd.h>
#include <experimental/filesystem>
#include "lib/finanzier/finanzierlib.h"
#include <ctime>
#include <libxml++/libxml++.h>
#include <fstream>
#include "config.h"
#include "lib/logger/logger.h"


class initialisation
{
    public:
    initialisation();
    ~initialisation();

    void set_save_folder(std::string new_save_folder);

    std::string get_year(){return std::to_string(this_year);};
    std::string get_month(){return this_month;};

    void run();

    private:

    int this_year;
    std::string this_month;
    std::string save_folder;

    void create_save_folder();
    void create_save_files();
    void create_month_folder();
};
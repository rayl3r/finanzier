#pragma once

#include <string>
#include <stdio.h>
#include <iostream>
#include "lib/finanzier/finanzierlib.h"
#include "initialisation.h"
#include "config.h"

class save_read
{   
    public:

    save_read();
    virtual ~save_read(){};

    virtual void set_save_folder(std::string new_save_folder){save_folder = new_save_folder;
                                                        save_file = save_folder + "/" + std::to_string(this_year) + "/" + this_month + ".xml";
                                                        vars_file = save_folder + "/" + std::to_string(this_year) + "/vars.xml";
                                                        month_file = save_folder + "/" + std::to_string(this_year) + "/monthly.xml";};

    protected:

    std::string this_month;
    int this_year;

    std::string save_folder;

    std::string save_file;
    std::string date;
    std::string vars_file;
    std::string month_file;
};

class save : public save_read
{
    public:
    save(){};
    ~save(){};

    void add_entry(std::string ammount, std::string reason);
    void remove_entry(std::string number);
    
    void update_numbers();

    void edit_ammount(std::string entry_number ,std::string new_ammount);
    void edit_reason(std::string entry_number ,std::string new_reason);

    void set_budget(std::string new_budget);

    void add_monthly(std::string amount, std::string reason);
    void remove_monthly(std::string number);
};


class reader : public save_read
{
    public:
    reader(){};
    ~reader(){};

    std::string read_ammount(std::string entry_number);
    std::string read_reason(std::string entry_number);
    std::string read_date(std::string entry_number);
    int get_last_entry_number();
    std::string get_string_from_xml(std::string entry_number);

    std::string get_budget();

    std::list<std::vector<std::string>> get_every_monthly();
};
